<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Function</title>
</head>
<body>
    <h1>Tukar Besar Kecil</h1>
    <?php 
    /* buatlah sebuah file dengan nama tentukan-nilai.php. 
    Di dalam file tersebut buatlah function dengan nama tentukan_nilai yang menerima 
    parameter berupa integer. dengan ketentuan jika paramater integer lebih besar 
    dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn String 
    “Sangat Baik” Selain itu jika parameter integer lebih besar sama dengan 70 dan 
    lebih kecil dari 85 maka akan mereturn string “Baik” selain itu jika parameter 
    number lebih besar sama dengan 60 dan lebih kecil dari 70 maka akan mereturn 
    string “Cukup” selain itu maka akan mereturn string “Kurang” */

    function tukar_besar_kecil($string){
        //kode di sini
        $abjad ="abcdefghijklmnopqrstuvwxyz";
        $output = "";

        for ($i = 0; $i< strlen($string); $i++){
            $huruf_kecil= strpos ($abjad, $string[$i]);
            if ($huruf_kecil == null){
                $output .= strtolower($string[$i]);
            }else{
                $output.= strtoupper($string[$i]);

            }
        }
            return "$output <br>";
    }     
      
        
        // TEST CASES
        echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
        echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
        echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
        echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
        echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"
         
    ?>
</body>
</html>
